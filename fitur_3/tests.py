from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
from unittest import skip

# Create your tests here.

class FiturLoginUnitTest(TestCase):

    def test_fitur_login_url_is_exist(self):
        response = Client().get('/fitur-3/')
        self.assertEqual(response.status_code,200)

    def test_fitur_login_using_index_func(self):
        found = resolve('/fitur-3/')
        self.assertEqual(found.func, index)