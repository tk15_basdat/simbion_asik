from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.views.generic.base import RedirectView

import fitur_1.urls as fitur_1
import fitur_2.urls as fitur_2
import fitur_3.urls as fitur_3
import fitur_4.urls as fitur_4
import fitur_5.urls as fitur_5
import fitur_6.urls as fitur_6
import fitur_7.urls as fitur_7
import fitur_8.urls as fitur_8
import fitur_9.urls as fitur_9

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(fitur_1, namespace='login')),
    url(r'^register/', include(fitur_2, namespace='register')),
    url(r'^fitur-3/', include(fitur_3, namespace='fitur-3')),
    url(r'^fitur-4/', include(fitur_4, namespace='fitur-4')),
    url(r'^fitur-5/', include(fitur_5, namespace='fitur-5')),
    url(r'^fitur-6/', include(fitur_6, namespace='fitur-6')),
    url(r'^tempat-wawancara/', include(fitur_7, namespace='tempat-wawancara')),
    url(r'^pengumuman/', include(fitur_8, namespace='pengumuman')),
    url(r'^pembayaran/', include(fitur_9, namespace='pembayaran'))
]
