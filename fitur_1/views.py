from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.contrib import messages
from django.urls import reverse

# Create your views here.
def index(request):
    return render(request, 'login.html')

# def register(request):
    return render(request, 'register.html')

def login(request):
    response = {}
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if request.method == 'POST':

            if (request.POST['username'] == 'mahasiswa' or request.POST['username'] == 'donatur' or request.POST['username'] == 'individual donatur' or request.POST['username'] == 'yayasan') and request.POST['password'] == 'simbion15':
                response['username'] = request.POST['username']
                request.session['username'] = request.POST['username']
                request.session['logged_in'] = True
                request.session.modified = True

                return redirect(reverse('fitur_1:login'))
            else:
                response['error_msg'] = 'Incorrect username or password.'
                response['username'] = request.POST['username']
                return render(request, 'login.html', response)
        else:
            return render(request, 'login.html')

    else:
        response['error_msg'] = 'Anda sudah masuk kedalam sistem.'
        return HttpResponseRedirect('fitur_4')
		# return render(request, '4 Informasi Beasiswa Aktif.html', response)

def logout(request):
    messages.info(request, 'Anda berhasil keluar dari sistem.')
    request.session.flush()
    return redirect(reverse('4 Informasi Beasiswa Aktif.html'))
