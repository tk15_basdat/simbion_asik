from django.conf.urls import url
from .views import index, login, logout
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login/$', login, name="login"),
    url(r'^logout/$', logout, name="logout"),
]
