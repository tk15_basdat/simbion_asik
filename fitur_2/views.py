# response = {'author':"Kelompok 15"}
# def index(request):
# 	response['author'] = "Kelompok 15"
# 	html = 'register.html'
# 	return render(request, html, response)

# def register_donatur(request):
#     response['author'] = "Kelompok 15"
#     html = 'register_donatur.html'
#     return render(request, html, response)

# def register_form_mhs(request):
#     html = 'register_form_mhs.html'
#     return render(request, html, response)

from django.shortcuts import render
# from mendaftarkan_skema_beasiswa.models import Beasiswa
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}
def index(request):
    if("username" not in request.session):
        return render(request, 'register.html', response)
    else:
        return HttpResponseRedirect(reverse('fitur_4'))

def role(request):
    if request.method == 'POST':
        role = request.POST.get('exampleFormControlSelect1')
        print("haha" + role)
        if(role=='Mahasiswa'):
            return HttpResponseRedirect('form_mhs.html')
        elif(role=='Donatur'):
            return HttpResponseRedirect('register_donatur.html')
        
def reg_as_donatur(request):
    return render(request, 'register_donatur.html',response)

def form_mhs(request):
    return render(request, 'form_mhs.html',response)

def form_ind_donatur(request):
    return render(request, 'form_ind_donatur.html',response)

def form_yayasan(request):
    return render(request, 'form_yayasan.html', response)

def reg_mhs(request):
    for a in request.POST:
        print(a + " : " + request.POST[a])

    return HttpResponseRedirect(reverse('login:index'))

def reg_ind_donatur(request):
    for a in request.POST:
        print(a + " : " + request.POST[a])

    return HttpResponseRedirect(reverse('login:index'))

def reg_yayasan(request):
    for a in request.POST:
        print(a + " : " + request.POST[a])

    return HttpResponseRedirect(reverse('login:index'))

def popup_notif_sukses(request):
    return render(request, 'popup_notif_sukses.html',response)