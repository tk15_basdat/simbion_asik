from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, register_donatur 
from django.http import HttpRequest
from unittest import skip

# Create your tests here.

class FiturRegisterUnitTest(TestCase):

    def test_fitur_register_url_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code,200)

    def test_fitur_register_using_index_func(self):
        found = resolve('/register/')
        self.assertEqual(found.func, index)

    def test_fitur_register_as_donatur_url_is_exist(self):
        response = Client().get('/register-donatur/')
        self.assertEqual(response.status_code,200)
    
    def test_fitur_register_form_mhs_url_is_exist(self):
        response = Client().get('/register/register-form-mhs/')
        self.assertEqual(response.status_code,200)