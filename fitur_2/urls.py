from django.conf.urls import url
from .views import index, role, reg_as_donatur, form_mhs, form_ind_donatur, form_yayasan, reg_mhs, reg_ind_donatur, reg_yayasan, popup_notif_sukses
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^role/$',role,name="role"),
    url(r'^reg-as-donatur/$',reg_as_donatur,name="reg_as_donatur"),
    url(r'^form-mhs/$',form_mhs,name='form_mhs'),
    url(r'^form-ind-donatur/$',form_ind_donatur,name='form_ind_donatur'),
    url(r'^form-yayasan/$',form_yayasan,name='form_yayasan'),
    url(r'^reg-mhs/$',reg_mhs,name='reg_mhs'),
    url(r'^reg-ind-donatur/$',reg_ind_donatur,name='reg_ind_donatur'),
    url(r'^reg-yayasan/$',reg_yayasan,name='reg_yayasan'),
    url(r'^popup-notif-sukses/$',popup_notif_sukses,name='popup_notif_sukses'),

]
